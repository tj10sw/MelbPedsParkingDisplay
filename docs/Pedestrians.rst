====================================
Melbourne Pedrestrians
====================================

---------------------------------------------
Overview
---------------------------------------------

Melbourne pedrestrian counts comes from `MCC data portal <https://data.melbourne.vic.gov.au/Transport/Pedestrian-Counting-System-2009-to-Present-counts-/b2ak-trbp>`_.

Counts grouped by week - daily mean.

.. image:: images/Melb_peds_per_week.png

Counts per day - daily mean.

.. image:: images/Melb_ped_per_day.png


A map of the location of each sensor is available `here <../_static/maps/MCC_Ped_sectors_2.html>`_. Click on a sensor to see a timeseries.

**Created**: 2020-05-28
**Updated**: 2020-06-11
