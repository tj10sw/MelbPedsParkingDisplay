====================================
Melbourne Parking
====================================

---------------------------------------------
Overview
---------------------------------------------

Counts grouped by week - daily mean.

.. image:: images/Melb_parking_per_week.png

**Created**: 2020-05-28
**Updated**: 2020-06-11
