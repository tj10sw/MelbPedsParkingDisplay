![Build Status](https://gitlab.com/pages/sphinx/badges/master/pipeline.svg)

---

Display of the outputs for Melbourne Pedestrian Counts and Melbourne Parking Counts.

This is part of a larger project to analyse data from various sources
to analyse the effect of the COVID-19 virus on Melbourne AU

The web page is visible at https://tj10sw.gitlab.io/MelbPedsParkingDisplay


The source code (private projects) is at :

* [Melbourne Pedestrian Counts](https://gitlab.com/tj10sw/melbourne-pedestrian-counts)
* [Melbourne Parking Counts](https://gitlab.com/tj10sw/melbourne-parking-counts)
