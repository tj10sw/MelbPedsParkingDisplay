.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Melbourne Pedestrian and Parking Changes Due to COVID-19
===============================================================

This is part of a larger project to analyse data from various sources
to analyse the effect of the COVID-19 virus on Melbourne AU.


Data upto 2020-06-10


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   docs/Parking
   docs/Pedestrians

